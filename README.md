Custom program management.
===========================

The goal of this program is to manage the startup and control of certain programs whose confiurations may be easily changed during development.  
This can be achieved easily enough with any scipting languages(bash, perl).  
This is done mostly to become familiar with program management and some boost libraries.  

Requirements
* Boost
   program options boost/program_options form boost libraries.  
   xml_parser for the config file

Usage: see --help

TODO:
* Fix the kill function and uncomment forks
* Add support for restarts
 

