/**\file startupRun.cc
 * \author keutoi(k3ut0i)
 * \brief start up program with pid management
 *
 * Easy enough to do in bash. Why am i bothering with cc?
 *
 * Well the main purpose of this program is to start the processes that aren't running
 * Usefull for xmonad etc .. restarts after conf changes.
 * Add custom scripts to this folder and start them with this program
 *
 * TODO: Add support for single process specified by -c -s -r options
 */
#include<stdio.h>
#include<sys/wait.h>
#include<unistd.h>

#include<iostream>
#include<map>


#include<boost/property_tree/ptree.hpp>
#include<boost/property_tree/xml_parser.hpp>
#include<boost/foreach.hpp>
#include<boost/program_options.hpp>



int main(int argc, char **argv)
{
    //Options
    namespace po = boost::program_options;
    po::options_description desc("Allowed Options");
    desc.add_options()
            ("help", "show help message")
            ("start,s",po::value<std::string>(), "start all")
            ("start-all,S", "start all")
            ("close,c", po::value<std::string>(), "close the program")
            ("close-all,C", "close all the programs")
            ("restart,r", po::value<std::string>(), "restart the program")
            ("restart-all,R", "restart all the programs")
        ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
        std::cout << desc <<"\n";
        return 1;
    }
    //Config file
    using boost::property_tree::ptree;
    ptree configtree;
    read_xml("./Config.xml", configtree);

    pid_t parent_pid = getpid();
    printf("Parent PID:\t%d\n", (int)parent_pid);

    BOOST_FOREACH(ptree::value_type const& v, configtree.get_child("config"))
        {

           if(v.first == "program")
           {
               //debug
               //printf("Program from Config.xml : %s\n", v.second.get<std::string>("name").c_str());
               //printf("\tCommand \t: %s\n", v.second.get<std::string>("command").c_str());
               //printf("\tArguments\t: %s\n", v.second.get<std::string>("argv").c_str());
               //
               std::string name     = v.second.get<std::string>("name");
               std::string command  = v.second.get<std::string>("command");
               std::string argv     = v.second.get<std::string>("argv");
               std::string pidfilename = "."+command+".pid";

               if(access(pidfilename.c_str(), R_OK) == -1)//the file does not exsist already
               {
                   if(vm.count("start-all"))
                   {
                        int pid = fork();
                        if(pid == 0)
                        {
                            pid_t child_pid = getpid();
                            printf("After fork, %s process id:\t%d\n", name.c_str(), (int)child_pid);
                            FILE *pidfile = fopen(pidfilename.c_str(), "w");
                            fprintf(pidfile,"%d", (int)child_pid);
                            fclose(pidfile);


                            //check it the program is running if not. check if .pid files exsist or pgrep it.
                            //if not create file
                            //execvp(command, argv);
                            //if error delete file
                            exit(0);
                        }
                   }
              }
               else //if the file already exsists
               {
                   if(vm.count("start-all"))
                        printf("%s is already running\n", name.c_str());
                   else if(vm.count("close-all"))
                   {
                        int proc_pid;
                        FILE *pidfile = fopen(pidfilename.c_str(), "r");
                        fscanf(pidfile,"%d", &proc_pid);
                        fclose(pidfile);
                        printf("Closing \t\t%s:%d\n", command.c_str(), proc_pid);
                        //check if the process is actually running kill(pid, 0)
                        //if running send SIGINT, SIGTERM
                        //wait, check again
                        //if still running send SIGKILL
                        remove(pidfilename.c_str());
                   }
               }

           }
        }
    //TODO:add support for argp
    //learn how to add debug flags

return 0;}

