
CC 	= gcc
CFLAGS += -Wall -Wextra -Wno-write-strings -std=c++11 
LDLIBS += -lstdc++ -lboost_program_options


all: build
	$(CC) $(CFLAGS) $(LDLIBS) -o build/startupRun startupRun.cc

build:
	mkdir -p build
clean:
	rm -rf build/
